const form = document.querySelector(".password-form");
const passwordInputs = form.querySelectorAll(".input-password");
const toggleEyeIcons = form.querySelectorAll(".icon-password");

const error = document.createElement("p");
passwordInputs[1].after(error);
error.innerHTML = "Потрібно ввести однакові значення";
error.style.cssText = `display: none;
    color: red;
    margin: 0 0 16px;`

toggleEyeIcons.forEach((icon) => {
    icon.addEventListener("click", (event) => {
        const inputWrapper = event.target.parentNode;
        const passwordInput = inputWrapper.querySelector(".input-password");
        passwordInput.type = passwordInput.type === "password" ? "text" : "password";
        icon.classList.toggle("fa-eye");
        icon.classList.toggle("fa-eye-slash");
    });
});

form.addEventListener("submit", (event) => {
    event.preventDefault();

    const passwordValue1 = passwordInputs[0].value;
    const passwordValue2 = passwordInputs[1].value;
    const error = form.querySelector("p");

    if (passwordValue1 === passwordValue2) {
        if (passwordValue1.length === 0 || passwordValue2.length === 0) {
            error.style.display = "inline-block";
        } else {
            error.style.display = "none";
            alert("You are welcome");
        }
    } else {
        error.style.display = "inline-block";
    }
});